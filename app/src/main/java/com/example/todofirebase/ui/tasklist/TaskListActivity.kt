package com.example.todofirebase.ui.tasklist

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.todofirebase.R
import com.example.todofirebase.model.Task
import com.example.todofirebase.ui._common.DataBindingActivity
import com.example.todofirebase.ui.location.LocationActivity
import com.example.todofirebase.ui.login.LoginActivity
import com.example.todofirebase.ui.taskdetail.TaskDetailActivity
import com.example.todofirebase.ui.tasklist.adapter.TaskAdapter
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_task_list.*
import javax.inject.Inject


private val TAG = TaskListActivity::class.java.simpleName

class TaskListActivity : DataBindingActivity<com.example.todofirebase.databinding.ActivityTaskListBinding>(),
    Navigation {

    private val WELCOME_MESSAGE_KEY = "welcome_message"
    private val WELCOME_MESSAGE_CAPS_KEY = "welcome_message_caps"

    private val REQUEST_ADD = 0
    private val REQUEST_UPDATE = 1

    override val layoutId = R.layout.activity_task_list

    @Inject
    lateinit var firebaseInstanceId: FirebaseInstanceId

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var firebaseRemoteConfig: FirebaseRemoteConfig

    private val viewModel by lazy {
        ViewModelProviders.of(
            this,
            viewModelFactory
        ).get(
            TaskListViewModel::class.java
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        val activity = this

        firebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults)

        getCurrentToken()

        with(binding) {
            lifecycleOwner = activity
            viewModel = activity.viewModel

            taskRV.layoutManager = LinearLayoutManager(activity)
            taskRV.adapter = TaskAdapter(activity.viewModel)
        }

        with(viewModel) {

            checkLogin()

            isSignedInEvent.observe(activity, Observer {
                it.getContentIfNothandled()?.let { s ->
                    s.let {
                        title = it
                    }
                }
            })

            needLoginEvent.observe(activity, Observer {
                it.getContentIfNothandled()?.let {
                    navigateLogin()
                }
            })

            updateTaskEvent.observe(activity, Observer {
                it.getContentIfNothandled()?.let {
                    onItemClickListener(it)
                }
            })

            fetchTaskList()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_task, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.addBTN -> {
                navigateTaskDetail()
            }
            R.id.logoutBTN -> {
                navigateLogin()
            }
            R.id.location -> {
                navigateToLocation()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun navigateTaskDetail() {
        startActivityForResult(
            TaskDetailActivity.intentForAddTask(this),
            REQUEST_ADD
        )
    }

    override fun navigateLogin() {
        startActivity(Intent(this, LoginActivity::class.java))
    }

    override fun onItemClickListener(task: Task) {
        startActivityForResult(
            TaskDetailActivity.intentForEditTask(this, task),
            REQUEST_UPDATE
        )
    }

    override fun navigateToLocation() {
        startActivity(
            Intent(
                this,
                LocationActivity::class.java
            )
        )
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_ADD -> {
                if (resultCode == Activity.RESULT_OK) {
                    viewModel.fetchTaskList()
                }
            }
            REQUEST_UPDATE -> {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    val updatedTask = data.getParcelableExtra<Task>(TaskDetailActivity.EXTRA_TASK)
                    viewModel.modifyList(updatedTask)
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun getCurrentToken(): com.google.android.gms.tasks.Task<InstanceIdResult> {
        return firebaseInstanceId.instanceId
            .addOnCompleteListener {
                if (!it.isSuccessful) {
                    return@addOnCompleteListener
                }
                val token = it.result?.token
                Toast.makeText(applicationContext, "$token", Toast.LENGTH_SHORT).show()
            }
    }
}