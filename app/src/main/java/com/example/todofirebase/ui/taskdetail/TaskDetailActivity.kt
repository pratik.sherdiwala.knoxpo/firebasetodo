package com.example.todofirebase.ui.taskdetail

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.todofirebase.BuildConfig
import com.example.todofirebase.R
import com.example.todofirebase.databinding.ActivityTaskDetailBinding
import com.example.todofirebase.model.Task
import com.example.todofirebase.ui._common.DataBindingActivity
import com.example.todofirebase.util.extensions.GlideApp
import dagger.android.AndroidInjection
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import javax.inject.Inject

class TaskDetailActivity : DataBindingActivity<ActivityTaskDetailBinding>() {

    companion object {
        private val TAG = TaskDetailActivity::class.java.simpleName

        private val EXTRA_ACTION = "$TAG.EXTRA_ACTION"
        private const val ACTION_ADD = 0
        private const val ACTION_UPDATE = 1

        private const val FILE_AUTHORITY = BuildConfig.FILES_AUTHORITY

        private const val REQUEST_IMAGE = 0

        val EXTRA_TASK = "$TAG.EXTRA_TASK"

        internal fun intentForAddTask(context: Context) = Intent(
            context,
            TaskDetailActivity::class.java
        ).apply {
            putExtra(EXTRA_ACTION, ACTION_ADD)
        }

        internal fun intentForEditTask(
            context: Context,
            task: Task
        ) = Intent(
            context,
            TaskDetailActivity::class.java
        ).apply {
            Log.d(TAG, "${task.id}")
            putExtra(EXTRA_ACTION, ACTION_UPDATE)
            putExtra(EXTRA_TASK, task)
        }

        internal fun addImageIntent(context: Context, list: MutableList<Intent>, intent: Intent): MutableList<Intent> {

            val reinfos = context.packageManager.queryIntentActivities(intent, 0)

            for (reinfo in reinfos) {
                val packageName = reinfo.activityInfo.packageName
                val targetIntent = Intent(intent)
                targetIntent.`package` = packageName
                list.add(targetIntent)
            }
            return list
        }
    }

    val task: Task
        get() = intent.getParcelableExtra(EXTRA_TASK)

    override
    val layoutId = R.layout.activity_task_detail

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy {
        ViewModelProviders.of(
            this,
            viewModelFactory
        ).get(
            TaskDetailViewModel::class.java
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        val activity = this
        with(binding) {

            viewModel = activity.viewModel

            actionBTN.setOnClickListener {
                when (intent.getIntExtra(EXTRA_ACTION, -1)) {
                    ACTION_UPDATE -> {
                        activity.viewModel.updateTask(task)
                    }
                    ACTION_ADD -> {
                        activity.viewModel.addTask()
                    }
                }
            }
        }

        when (intent.getIntExtra(EXTRA_ACTION, -1)) {
            ACTION_ADD -> {
                binding.actionBTN.text = "ADD"
            }
            ACTION_UPDATE -> {
                updateUI()
                binding.actionBTN.text = "UPDATE"
            }
        }

        with(viewModel) {

            file.observe(activity, Observer {
                GlideApp.with(activity)
                    .load(it)
                    .centerCrop()
                    .into(binding.taskImageIV)
            })

            pickImageEvent.observe(activity, Observer {
                it.getContentIfNothandled()?.let {
                    activity.pickImage()
                }
            })

            taskAddedEvent.observe(activity, Observer {
                it.getContentIfNothandled()?.let {
                    Toast.makeText(activity, "Task added", Toast.LENGTH_SHORT).show()
                    setResult(Activity.RESULT_OK)
                    finish()
                }
            })

            errorTaskAddEvent.observe(activity, Observer {
                it.getContentIfNothandled()?.let {
                    Toast.makeText(activity, "Task cant be add", Toast.LENGTH_SHORT).show()
                }
            })

            taskUpdateEvent.observe(activity, Observer {
                it.getContentIfNothandled()?.let {
                    setResult(Activity.RESULT_OK, Intent().putExtra(EXTRA_TASK, it))
                    finish()
                }
            })

            fieldsEmptyEvent.observe(activity, Observer {
                it.getContentIfNothandled()?.let {
                    Toast.makeText(applicationContext, "Fields Cant be Empty", Toast.LENGTH_SHORT).show()
                }
            })
        }
    }

    private fun updateUI() {
        with(viewModel) {
            title.value = task.taskTitle
            description.value = task.taskDescription
            isCompleted.value = task.taskStatus
        }

        GlideApp.with(baseContext)
            .load(task.photoUrl)
            .error(R.mipmap.ic_launcher)
            .centerCrop()
            .into(binding.taskImageIV)
    }

    private fun pickImage() {

        var intentList = mutableListOf<Intent>()

        Intent(Intent.ACTION_PICK).apply {
            type = "image/jpg"
        }.also {
            intentList = addImageIntent(
                this,
                intentList,
                it
            )
        }

        Intent(MediaStore.ACTION_IMAGE_CAPTURE_SECURE).also { takePicIntent ->
            takePicIntent.resolveActivity(this.packageManager)?.also {
                val photoFile: File? = try {
                    viewModel.getFileForCamera()
                } catch (e: IOException) {
                    null
                }

                photoFile?.also {
                    val photoUri = FileProvider.getUriForFile(
                        this,
                        FILE_AUTHORITY,
                        it
                    )
                    takePicIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
                }
            }
        }.also {
            intentList = addImageIntent(
                this,
                intentList,
                it
            )
        }

        Intent.createChooser(
            intentList.removeAt(intentList.size - 1),
            "TITLE"
        ).run {
            putExtra(Intent.EXTRA_INITIAL_INTENTS, intentList.toTypedArray())
            startActivityForResult(this, REQUEST_IMAGE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_IMAGE -> {
                if (resultCode != Activity.RESULT_OK) {
                    return
                }
                Log.d(TAG, "data: $data")
                (data?.data
                    ?: (
                            if (viewModel.getExistingCameraFile()?.exists() == true) {
                                Uri.fromFile(viewModel.getExistingCameraFile())
                            } else null
                            )
                        )?.let {
                    try {
                        contentResolver.openFileDescriptor(it, "r")
                    } catch (e: IOException) {
                        null
                    }
                        ?.let {
                            val inputStream = FileInputStream(it.fileDescriptor)
                            viewModel.saveFile(inputStream)
                        }
                }
            }
        }
    }
}