package com.example.todofirebase.ui._common

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.todofirebase.model.Task
import com.example.todofirebase.ui.tasklist.adapter.TaskAdapter

object DataBindingAdapter {
    @JvmStatic
    @BindingAdapter("task")
    fun setTask(view: RecyclerView, tasks: List<Task>?) {
        tasks?.let {
            (view.adapter as TaskAdapter).updateTask(it)
        }
    }
}