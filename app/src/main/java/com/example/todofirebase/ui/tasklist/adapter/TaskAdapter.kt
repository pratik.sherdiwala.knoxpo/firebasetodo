package com.example.todofirebase.ui.tasklist.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.todofirebase.R
import com.example.todofirebase.model.Task
import com.example.todofirebase.ui.tasklist.TaskListViewModel
import com.example.todofirebase.ui.tasklist.adapter.viewholder.TaskVH
import javax.inject.Inject

class TaskAdapter @Inject constructor(
    private val taskListViewModel: TaskListViewModel
) : RecyclerView.Adapter<TaskVH>() {

    private var taskList: List<Task>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskVH {
        return TaskVH(
            LayoutInflater.from(parent.context)
                .inflate(
                    R.layout.item_task,
                    parent,
                    false
                )
        )
    }

    override fun getItemCount() = taskList?.size ?: 0

    override fun onBindViewHolder(holder: TaskVH, position: Int) {

        holder.bindTask(taskList!![position],
            {
                taskListViewModel.deleteTask(it)
            },
            {
                taskListViewModel.updateTask(it)
            }
        )

    }

    fun updateTask(newtask: List<Task>) {
        taskList = newtask
        notifyDataSetChanged()
    }
}