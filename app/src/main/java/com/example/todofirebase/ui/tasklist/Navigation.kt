package com.example.todofirebase.ui.tasklist

import com.example.todofirebase.model.Task

interface Navigation {
    fun navigateTaskDetail()
    fun navigateLogin()
    fun onItemClickListener(task:Task)
    fun navigateToLocation()
}