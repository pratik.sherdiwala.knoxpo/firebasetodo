package com.example.todofirebase.ui.taskdetail

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.todofirebase.Event
import com.example.todofirebase.data.FileRepository
import com.example.todofirebase.data.TaskRepository
import com.example.todofirebase.model.Task
import com.google.firebase.Timestamp
import io.reactivex.disposables.CompositeDisposable
import java.io.File
import java.io.FileInputStream
import javax.inject.Inject

private val TAG = TaskDetailViewModel::class.java.simpleName

class TaskDetailViewModel @Inject constructor(
    private val taskRepository: TaskRepository,
    private val fileRepository: FileRepository
) : ViewModel() {

    val title = MutableLiveData<String>()
    val description = MutableLiveData<String>()
    val isCompleted = MutableLiveData<Boolean>()

    var file = MutableLiveData<File>()

    private val _taskUpdateEvent = MutableLiveData<Event<Task>>()
    val taskUpdateEvent: LiveData<Event<Task>>
        get() = _taskUpdateEvent

    private val _taskAddedEvent = MutableLiveData<Event<Unit>>()
    val taskAddedEvent: LiveData<Event<Unit>>
        get() = _taskAddedEvent

    private val _errorTaskAddEvent = MutableLiveData<Event<Unit>>()
    val errorTaskAddEvent: LiveData<Event<Unit>>
        get() = _errorTaskAddEvent

    private val _pickImageEvent = MutableLiveData<Event<Unit>>()
    val pickImageEvent: LiveData<Event<Unit>>
        get() = _pickImageEvent

    private val _fieldsEmptyEvent = MutableLiveData<Event<Unit>>()
    val fieldsEmptyEvent: LiveData<Event<Unit>>
        get() = _fieldsEmptyEvent

    private val disposables = CompositeDisposable()

    var photoUrl: String? = null

    fun addTask() {

        if (title.value == null || description.value == null) {
            _fieldsEmptyEvent.value=Event(Unit)
            return
        }

        if (file.value != null) {

            fileRepository.uploadImage(file.value!!)
                .flatMapCompletable {
                    val task = Task()
                    task.taskTitle = title.value!!
                    task.taskDescription = description.value!!
                    task.taskStatus = isCompleted.value ?: false
                    task.date = Timestamp.now()
                    task.photoUrl = it.toString()

                    taskRepository.addTask(task)
                }
                .subscribe(
                    {
                        _taskAddedEvent.value = Event(Unit)
                    },
                    {
                        Log.e(TAG, "Error while uploading image")
                    }
                )
                .also {
                    disposables.add(it)
                }
        } else {
            val task = Task()
            task.taskTitle = title.value!!
            task.taskDescription = description.value!!
            task.taskStatus = isCompleted.value ?: false
            task.date = Timestamp.now()
            task.photoUrl = ""

            taskRepository.addTask(task)
                .subscribe(
                    {
                        _taskAddedEvent.value = Event(Unit)
                    },
                    {
                        Log.d(TAG, "Error while adding task")
                    }
                ).also {
                    disposables.add(it)
                }

        }
    }

    fun updateTask(task: Task) {
        task.taskTitle = title.value
        task.taskDescription = description.value
        task.taskStatus = isCompleted.value ?: false

        Log.d(TAG, "${task.id}")

        taskRepository.updateTask(task)
            .subscribe(
                {
                    _taskUpdateEvent.value = Event(task)
                },
                {
                    Log.e(TAG, "Error while updating task", it)
                }
            ).also {
                disposables.add(it)
            }
    }

    fun saveFileToServer() {
        if (file.value != null)
            fileRepository.uploadImage(file.value!!)
                .subscribe(
                    {
                        Log.d(TAG, "File Uploaded: ${it}")
                        photoUrl = it.toString()
                    },
                    {
                        Log.e(TAG, "Uploading File Error")
                    }
                )
                .also {
                    disposables.add(it)
                }
    }

    fun pickImage() {
        _pickImageEvent.value = Event(Unit)
    }

    fun getFileForCamera(): File {
        file.value = fileRepository.createImageFile()
        return file.value!!
    }

    fun getExistingCameraFile(): File? {
        return file.value
    }

    fun saveFile(inputStream: FileInputStream) {
        file.value = fileRepository.saveFile(inputStream)
        Log.d(TAG, "Saved File ${file.value!!.name}")
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }
}