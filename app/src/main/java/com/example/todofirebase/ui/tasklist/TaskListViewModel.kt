package com.example.todofirebase.ui.tasklist

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.todofirebase.Event
import com.example.todofirebase.data.AppRepository
import com.example.todofirebase.data.TaskRepository
import com.example.todofirebase.data.UserRepository
import com.example.todofirebase.model.Task
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

private val TAG = TaskListViewModel::class.java.simpleName

class TaskListViewModel @Inject constructor(
    private val taskRepository: TaskRepository,
    private val userRepository: UserRepository,
    private val appRepository: AppRepository
) : ViewModel() {

    val taskList = MutableLiveData<List<Task>>()

    private val _isSignedInEvent = MutableLiveData<Event<String>>()
    val isSignedInEvent: LiveData<Event<String>>
        get() = _isSignedInEvent

    private val _needLoginEvent = MutableLiveData<Event<Unit>>()
    val needLoginEvent: LiveData<Event<Unit>>
        get() = _needLoginEvent

    private val _updateTaskEvent = MutableLiveData<Event<Task>>()
    val updateTaskEvent: LiveData<Event<Task>>
        get() = _updateTaskEvent

    private val _checkRemoteConfig = MutableLiveData<Event<Boolean>>()
    val checkRemoteConfig: LiveData<Event<Boolean>>
        get() = _checkRemoteConfig

    private val disposables = CompositeDisposable()

    private var index: Int? = 0

    fun checkLogin() {
        userRepository.isLoggedIn()
            .subscribe(
                {
                    _isSignedInEvent.value = Event(it)
                },
                {
                    _needLoginEvent.value = Event(Unit)
                }
            ).also {

            }
    }

    fun fetchTaskList() {
        taskRepository.getAllTask().subscribe(
            {
                taskList.value = it
            },
            {
                Log.e(TAG, "Unable to fetch Task list", it)
            }
        ).also {
            disposables.add(it)
        }
    }

    fun deleteTask(task: Task) {
        taskRepository.deleteTask(task).subscribe(
            {
                val modifiedList = taskList.value?.toMutableList()
                modifiedList?.remove(task)

                taskList.value = modifiedList
            },
            {
                Log.e(TAG, "Task Cant be deleted", it)
            }
        ).also {
            disposables.add(it)
        }
    }

    fun updateTask(task: Task) {
        index = taskList.value!!.indexOf(task)
        _updateTaskEvent.value = Event(task)
    }

    fun modifyList(task: Task) {
        val modifiedList = taskList.value?.toMutableList()
        modifiedList?.set(index!!, task)
        taskList.value = modifiedList
    }

    private fun remoteConfig() {
        appRepository.checkFirebaseRemoteConfig()
            .subscribe(
                {
                    _checkRemoteConfig.value = Event(it)
                },
                {
                    Log.e(TAG, "Error")
                }
            ).also {
                disposables.add(it)
            }
    }
}