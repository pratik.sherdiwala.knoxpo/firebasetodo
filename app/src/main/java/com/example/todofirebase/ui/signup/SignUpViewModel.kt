package com.example.todofirebase.ui.signup

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.todofirebase.Event
import com.example.todofirebase.data.UserRepository
import com.example.todofirebase.model.User
import javax.inject.Inject

private val TAG = SignUpViewModel::class.java.simpleName

class SignUpViewModel @Inject constructor(
    private val userRepository: UserRepository
) : ViewModel() {

    val name = MutableLiveData<String>()
    val contact = MutableLiveData<String>()
    val address = MutableLiveData<String>()
    val email = MutableLiveData<String>()
    val password = MutableLiveData<String>()

    private val _signUpFailureEvent = MutableLiveData<Event<Unit>>()
    val signUpFailureEvent: LiveData<Event<Unit>>
        get() = _signUpFailureEvent

    private val _signUpSuccessEvent = MutableLiveData<Event<Unit>>()
    val signUpSuccessEvent: LiveData<Event<Unit>>
        get() = _signUpSuccessEvent

    private val _invalidInputEvent = MutableLiveData<Event<Unit>>()
    val invalidInputEvent: LiveData<Event<Unit>>
        get() = _invalidInputEvent

    fun signUp() {
        if (name.value != null || contact.value != null || address.value != null || email.value != null || password.value != null) {
            val user = User(
                name.value!!,
                contact.value!!,
                address.value!!,
                email.value!!,
                password.value!!
            )
            userRepository.signUp(user)
                .subscribe(
                    {
                        _signUpSuccessEvent.value = Event(Unit)
                    },
                    {
                        _signUpFailureEvent.value = Event(Unit)
                        Log.e(TAG, "Unable to sign up", it)
                    }
                ).also {

                }
        } else {
            _invalidInputEvent.value = Event(Unit)
        }
    }
}