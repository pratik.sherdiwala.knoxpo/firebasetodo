package com.example.todofirebase.ui.login

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.todofirebase.Event
import com.example.todofirebase.data.UserRepository
import javax.inject.Inject

private val TAG=LoginViewModel::class.java.simpleName

class LoginViewModel @Inject constructor(
    private val userRepository: UserRepository
) : ViewModel() {

    val username = MutableLiveData<String>()
    val password = MutableLiveData<String>()

    private val _showSignUpEvent = MutableLiveData<Event<Unit>>()
    val showSignUpEvent: LiveData<Event<Unit>>
        get() = _showSignUpEvent

    private val _loginSuccessEvent = MutableLiveData<Event<Unit>>()
    val loginSuccessEvent: LiveData<Event<Unit>>
        get() = _loginSuccessEvent

    private val _loginFailureEvent = MutableLiveData<Event<Unit>>()
    val loginFailureEvent: LiveData<Event<Unit>>
        get() = _loginFailureEvent

    fun signIn() {
        Log.d(TAG,"Button Clicked")
        if (username.value != null && password.value != null) {
            userRepository.signIn(username.value!!, password.value!!)
                .subscribe(
                    {
                        _loginSuccessEvent.value= Event(Unit)
                    },
                    {
                        _loginFailureEvent.value= Event(Unit)
                    }
                ).also {

                }
        }
    }

    fun createAccount(){
        _showSignUpEvent.value= Event(Unit)
    }
}