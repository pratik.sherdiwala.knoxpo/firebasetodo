package com.example.todofirebase.ui.location

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.todofirebase.R
import com.example.todofirebase.util.extensions.services.GpsService

class LocationActivity : AppCompatActivity() {
    lateinit var fragment: Fragment

    private lateinit var startBtn: Button
    private lateinit var stopBtn: Button
    private var broadcastReceiver: BroadcastReceiver? = null
    lateinit var cordinatesTV: TextView

    companion object {
        const val REQUEST_LOCATION = 100
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location)
        startBtn = findViewById(R.id.startBTN)
        stopBtn = findViewById(R.id.stopBTN)
        cordinatesTV = findViewById(R.id.cordinatesTV)

        if (!runTimePermission()) {
            enableButtons()
        }
    }

    override fun onResume() {
        super.onResume()
        if (broadcastReceiver == null) {
            broadcastReceiver = object : BroadcastReceiver() {
                override fun onReceive(context: Context?, intent: Intent?) {
                    cordinatesTV.append("\n ${intent?.extras?.get(GpsService.CORDINATES)}")
                }
            }
        }
        registerReceiver(broadcastReceiver, IntentFilter(GpsService.LOCATION_UPDATES))
    }

    override fun onDestroy() {
        super.onDestroy()
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver)
        }
    }

    private fun runTimePermission(): Boolean {
        if (Build.VERSION.SDK_INT >= 23 &&
            ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED &&
            ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ),
                REQUEST_LOCATION
            )
            return true
        }
        return false
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_LOCATION -> {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    enableButtons()
                } else {
                    runTimePermission()
                }
            }
        }
    }

    private fun enableButtons() {
        startBtn.setOnClickListener {
            Toast.makeText(this, "Starts", Toast.LENGTH_SHORT).show()
            startService(
                Intent(this, GpsService::class.java)
            )
        }

        stopBtn.setOnClickListener {
            Toast.makeText(this, "Stop", Toast.LENGTH_SHORT).show()
            stopService(
                Intent(this, GpsService::class.java)
            )
        }
    }
}