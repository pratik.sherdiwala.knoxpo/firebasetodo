package com.example.todofirebase.ui._common;

interface Listener {
    String getData();
}

class Demo implements Listener {

    @Override
    public String getData() {
        return "Hello From Demo";
    }
}

class Receiver {

    static String result = null;

    static void doSomething(Listener listener) {
        if (listener != null) {
            result = listener.getData();
            System.out.println(result);
        }
    }

    public static void main(String[] args) {

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                doSomething(new Demo());
            }
        });
        t.start();
    }
}
