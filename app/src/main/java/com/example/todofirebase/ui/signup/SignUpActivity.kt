package com.example.todofirebase.ui.signup

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.todofirebase.R
import com.example.todofirebase.databinding.ActivitySignUpBinding
import com.example.todofirebase.ui._common.DataBindingActivity
import com.example.todofirebase.ui.tasklist.TaskListActivity
import dagger.android.AndroidInjection
import javax.inject.Inject

class SignUpActivity : DataBindingActivity<ActivitySignUpBinding>(), Navigation {

    override val layoutId = R.layout.activity_sign_up

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy {
        ViewModelProviders.of(
            this, viewModelFactory
        ).get(SignUpViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        val activity = this

        with(binding) {
            lifecycleOwner = activity
            viewModel = activity.viewModel
        }

        with(viewModel) {
            signUpSuccessEvent.observe(activity, Observer {
                it.getContentIfNothandled()?.let {
                    navigateTaskList()
                }
            })

            signUpFailureEvent.observe(activity, Observer {
                it.getContentIfNothandled()?.let {
                    Toast.makeText(activity, "Account Creation Failure", Toast.LENGTH_SHORT).show()
                }
            })

            invalidInputEvent.observe(activity, Observer {
                it.getContentIfNothandled()?.let {
                    Toast.makeText(activity, "Field Can't be Empty", Toast.LENGTH_SHORT).show()
                }
            })
        }
    }

    override fun navigateTaskList() {
        startActivity(Intent(this, TaskListActivity::class.java))
    }

}