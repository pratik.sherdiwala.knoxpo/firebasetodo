package com.example.todofirebase.ui.signup

interface Navigation {
    fun navigateTaskList()
}