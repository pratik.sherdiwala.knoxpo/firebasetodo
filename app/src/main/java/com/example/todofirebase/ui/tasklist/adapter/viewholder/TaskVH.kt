package com.example.todofirebase.ui.tasklist.adapter.viewholder

import android.app.ActivityOptions
import android.view.View
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.core.app.ActivityOptionsCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.todofirebase.R
import com.example.todofirebase.model.Task
import com.example.todofirebase.util.extensions.GlideApp
import java.text.SimpleDateFormat
import java.util.*

class TaskVH(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val mTaskTitleTV = itemView.findViewById<TextView>(R.id.taskNameTV)
    private val mTaskDescTV = itemView.findViewById<TextView>(R.id.taskDescriptionTV)
    private val mTaskCB = itemView.findViewById<CheckBox>(R.id.taskStatusCB)
    private val mTaskDateTV = itemView.findViewById<TextView>(R.id.dateTV)
    private val mTaskDeleteBTN = itemView.findViewById<ImageView>(R.id.removeBTN)
    private val mTaskImageIV = itemView.findViewById<ImageView>(R.id.taskImageIV)

    fun bindTask(
        task: Task,
        taskDelete: (task: Task) -> Unit,
        onItemClick: (task: Task) -> Unit
    ) {
        mTaskTitleTV.text = task.taskTitle
        mTaskDescTV.text = task.taskDescription
        mTaskCB.isChecked = task.taskStatus
        mTaskDateTV.text = dateFormat(task.date!!.toDate())

        GlideApp.with(itemView)
            .load(task.photoUrl)
            .error(R.mipmap.ic_launcher)
            .centerCrop()
            .into(mTaskImageIV)

        mTaskDeleteBTN.setOnClickListener {
            taskDelete(task)
        }

        itemView.setOnClickListener {
            onItemClick(task)
        }
    }

    private fun dateFormat(date: Date): String? {
        val format = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
        return format.format(date)
    }
}