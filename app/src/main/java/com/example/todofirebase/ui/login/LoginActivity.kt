package com.example.todofirebase.ui.login

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.todofirebase.R
import com.example.todofirebase.databinding.ActivityLoginBinding
import com.example.todofirebase.ui._common.DataBindingActivity
import com.example.todofirebase.ui.signup.SignUpActivity
import com.example.todofirebase.ui.tasklist.TaskListActivity
import dagger.android.AndroidInjection
import javax.inject.Inject

class LoginActivity : DataBindingActivity<ActivityLoginBinding>(), Navigation {

    override val layoutId = R.layout.activity_login

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy {
        ViewModelProviders.of(
            this,
            viewModelFactory
        )
            .get(LoginViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        val activity = this

        with(binding) {
            lifecycleOwner = activity
            viewModel = activity.viewModel
        }

        with(viewModel) {
            showSignUpEvent.observe(activity, Observer {
                it.getContentIfNothandled()?.let {
                    navigateSignUp()
                }
            })

            loginSuccessEvent.observe(activity, Observer {
                it.getContentIfNothandled()?.let {
                    navigateTaskList()
                }
            })
        }
    }

    override fun navigateSignUp() {
        startActivity(Intent(this, SignUpActivity::class.java))
    }

    override fun navigateTaskList() {
        startActivity(Intent(this, TaskListActivity::class.java))
    }
}