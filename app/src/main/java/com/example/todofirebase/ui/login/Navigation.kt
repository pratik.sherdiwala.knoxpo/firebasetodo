package com.example.todofirebase.ui.login

interface Navigation {
    fun navigateSignUp()
    fun navigateTaskList()
}