package com.example.todofirebase.util.extensions.services

import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.os.IBinder
import android.provider.Settings

class GpsService : Service() {

    companion object {
        val TAG = GpsService::class.java.simpleName
        val LOCATION_UPDATES = "$TAG.LOCATION_UPDATES"

        val CORDINATES = "$TAG.CORDINATES"
    }

    lateinit var locationListener: LocationListener
    lateinit var locationManager: LocationManager

    override fun onBind(intent: Intent?): IBinder? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    @SuppressLint("MissingPermission")
    override fun onCreate() {
        super.onCreate()
        locationListener = object : LocationListener {
            override fun onLocationChanged(location: Location?) {
                sendBroadcast(
                    Intent(LOCATION_UPDATES).apply {
                        putExtra(CORDINATES, "${location?.latitude} ${location?.longitude}")
                    }
                )
            }

            override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onProviderEnabled(provider: String?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onProviderDisabled(provider: String) {
                startActivity(
                    Intent(
                        Settings.ACTION_LOCATION_SOURCE_SETTINGS
                    ).apply {
                        flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    }
                )
            }

        }

        locationManager = applicationContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000L, 0f, locationListener)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (locationManager != null) {
            locationManager.removeUpdates(locationListener)
        }
    }
}