package com.example.todofirebase.util.extensions

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class TodoGlideModule : AppGlideModule()