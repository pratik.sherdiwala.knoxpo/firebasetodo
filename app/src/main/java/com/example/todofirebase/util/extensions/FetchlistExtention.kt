package com.example.todofirebase.util.extensions

import android.util.Log
import com.google.android.gms.tasks.Task
import io.reactivex.BackpressureStrategy
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

inline fun <reified T> Task<T>.toFlowable(): Flowable<T> {

    return Flowable.create(
        { flowable ->
            this.addOnSuccessListener {
                flowable.onNext(it)
                flowable.onComplete()
            }.addOnFailureListener {
                flowable.onError(it)
            }
        },
        BackpressureStrategy.LATEST
    )
}

inline fun <reified T> Task<T>.toSingle(): Single<T> {
    return Single.create { singleEmmiter ->

        this
            .addOnCompleteListener {
                Log.d("TAG", "onComplete $it")
            }
            .addOnSuccessListener {
                singleEmmiter.onSuccess(it)
            }
            .addOnFailureListener {
                singleEmmiter.onError(it)
            }
    }
}

fun <T> Task<T>.toCompletable(): Completable {
    return Completable.create { emitter ->

        addOnSuccessListener {
            emitter.onComplete()
        }
            .addOnFailureListener {
                emitter.onError(it)
            }
    }
}