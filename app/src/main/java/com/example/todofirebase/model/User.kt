package com.example.todofirebase.model

data class User(
    var name: String,
    var contact: String,
    var address: String,
    var email: String,
    var password: String
)
