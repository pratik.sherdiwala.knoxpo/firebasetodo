package com.example.todofirebase.model

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentSnapshot

class Task() : Parcelable {
    var taskTitle: String? = null
    var taskDescription: String? = null
    var taskStatus: Boolean = false
    var date: Timestamp? = null
    var id: String? = null
    var photoUrl: String? = null

    constructor(parcel: Parcel) : this() {
        taskTitle = parcel.readString()
        taskDescription = parcel.readString()
        taskStatus = parcel.readByte() != 0.toByte()
        date = parcel.readParcelable(Timestamp::class.java.classLoader)
        id = parcel.readString()
    }

    constructor(documentSnapshot: DocumentSnapshot) : this() {
        taskTitle = documentSnapshot["taskTitle"] as String
        taskDescription = documentSnapshot["taskDescription"] as String
        taskStatus = documentSnapshot["taskStatus"] as Boolean
        date = documentSnapshot["date"] as Timestamp
        id = documentSnapshot.id
        photoUrl = documentSnapshot["photoUrl"] as String
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(taskTitle)
        parcel.writeString(taskDescription)
        parcel.writeByte(if (taskStatus) 1 else 0)
        parcel.writeParcelable(date, flags)
        parcel.writeString(id)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Task> {
        override fun createFromParcel(parcel: Parcel): Task {
            return Task(parcel)
        }

        override fun newArray(size: Int): Array<Task?> {
            return arrayOfNulls(size)
        }
    }
}

