package com.example.todofirebase.di.module

import com.example.todofirebase.ui.login.LoginActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class LoginActivityModule {

    @ContributesAndroidInjector
    abstract fun contributeLoginActivity():LoginActivity

}