package com.example.todofirebase.di.module

import com.example.todofirebase.ui.tasklist.TaskListActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class TaskListActivityModule {
    @ContributesAndroidInjector
    abstract fun contributeTaskListActivity():TaskListActivity
}