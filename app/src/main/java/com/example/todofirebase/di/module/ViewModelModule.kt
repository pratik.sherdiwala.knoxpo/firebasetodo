package com.example.todofirebase.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.todofirebase.ui.login.LoginViewModel
import com.example.todofirebase.ui.signup.SignUpViewModel
import com.example.todofirebase.ui.taskdetail.TaskDetailViewModel
import com.example.todofirebase.ui.tasklist.TaskListViewModel
import com.example.todofirebase.viewmodel.AppViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(TaskListViewModel::class)
    abstract fun bindTaskListViewModel(viewModel: TaskListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TaskDetailViewModel::class)
    abstract fun bindTaskDetailviewModel(viewModel: TaskDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(viewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SignUpViewModel::class)
    abstract fun bindSignUpViewModel(viewModel: SignUpViewModel): ViewModel


    @Binds
    abstract fun provideAppviewModelFactory(appViewModelFactory: AppViewModelFactory): ViewModelProvider.Factory
}