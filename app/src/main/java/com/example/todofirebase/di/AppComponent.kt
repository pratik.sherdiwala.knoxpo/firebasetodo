package com.example.todofirebase.di

import android.app.Application
import com.example.todofirebase.App
import com.example.todofirebase.di.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AndroidSupportInjectionModule::class,
        ViewModelModule::class,
        TaskListActivityModule::class,
        TaskDetailActivityModule::class,
        AppModule::class,
        LoginActivityModule::class,
        SignUpActivityModule::class
    ]
)

interface AppComponent {
    @Component.Builder
    interface Builder{
        @BindsInstance
        fun context(context:Application):Builder
        fun create():AppComponent
    }
    fun inject(app: App)
}