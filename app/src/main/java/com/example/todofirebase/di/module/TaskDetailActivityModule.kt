package com.example.todofirebase.di.module

import com.example.todofirebase.ui.taskdetail.TaskDetailActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class TaskDetailActivityModule {

    @ContributesAndroidInjector
    abstract fun contributeTaskDetailActivity():TaskDetailActivity

}