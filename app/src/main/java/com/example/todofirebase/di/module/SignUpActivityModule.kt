package com.example.todofirebase.di.module

import com.example.todofirebase.ui.signup.SignUpActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class SignUpActivityModule {

    @ContributesAndroidInjector
    abstract fun contributeSignUpActivity():SignUpActivity

}