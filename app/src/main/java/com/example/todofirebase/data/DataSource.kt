package com.example.todofirebase.data

import com.example.todofirebase.model.Task
import io.reactivex.Completable
import io.reactivex.Single

interface DataSource {

    fun addTask(task: Task): Completable

    fun getAllTask(): Single<List<Task>>

    fun deleteTask(task: Task): Completable

    fun updateTask(task: Task): Completable
}