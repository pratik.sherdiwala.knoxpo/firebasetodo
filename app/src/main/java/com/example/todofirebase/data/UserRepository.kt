package com.example.todofirebase.data

import android.util.Log
import com.example.todofirebase.model.User
import com.example.todofirebase.util.extensions.toCompletable
import com.example.todofirebase.util.extensions.toFlowable
import com.example.todofirebase.util.extensions.toSingle
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

private val TAG = UserRepository::class.java.simpleName

class UserRepository @Inject constructor(
    private val firebaseAuth: FirebaseAuth,
    private val firebaseFirestore: FirebaseFirestore,
    private val firebaseInstanceId: FirebaseInstanceId
) {

    fun isLoggedIn(): Single<String> {
        val user = firebaseAuth.currentUser
        if (user != null) {
            return Single.just(user.email)
        }
        return Single.error(IllegalArgumentException("User Not Found"))
    }

    fun signIn(username: String, password: String): Flowable<AuthResult> {
        return firebaseAuth.signInWithEmailAndPassword(username, password)
            .addOnCompleteListener {
                Log.d(TAG, "Login Successful")
            }
            .addOnFailureListener {
                Log.d(TAG, "Login Failed")
            }
            .toFlowable()
            .map {
                it
            }
    }

    fun signUp(user: User): Completable {

        return firebaseAuth
            .createUserWithEmailAndPassword(user.email, user.password)
            .toSingle()
            .flatMapCompletable {
                firebaseFirestore
                    .collection("users")
                    .document(it.user.uid)
                    .set(user)
                    .toCompletable()
            }
    }

    fun getCurrentToken(): Task<InstanceIdResult> {
        return firebaseInstanceId.instanceId
            .addOnCompleteListener {
                if (!it.isSuccessful) {
                    return@addOnCompleteListener
                }
                it.result?.token
            }
    }
}