package com.example.todofirebase.data

import android.util.Log
import com.example.todofirebase.model.Task
import com.example.todofirebase.util.extensions.toCompletable
import com.example.todofirebase.util.extensions.toSingle
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

private val TAG = TaskRepository::class.java.simpleName

class TaskRepository @Inject constructor(
    private val dataSource: DataSource,
    private val firebaseFirestore: FirebaseFirestore,
    private val firebaseAuth: FirebaseAuth
) : ITaskRepository {

    override fun addTask(task: Task): Completable {
        if (firebaseAuth.currentUser == null) {
            return Completable.error(IllegalArgumentException("User not available"))
        }
        //users/currentUser.uid/tasks
        val dbTask = firebaseFirestore
            .collection("users")
            .document(firebaseAuth.currentUser!!.uid)
            .collection("tasks")
            .document()

        Log.d(TAG, "Going to add task")

        return dbTask.set(task)
            .toCompletable()

    }

    override fun getAllTask(): Single<List<Task>> {

        if (firebaseAuth.currentUser == null) {
            return Single.error(java.lang.IllegalArgumentException())
        }

        return firebaseFirestore
            .collection("users")
            .document(firebaseAuth.currentUser!!.uid)
            .collection("tasks")
            .orderBy("date")
            .get()
            .toSingle()
            .map {
                it.documents.map {
                    Task(it)
                }
            }
    }

    override fun deleteTask(task: Task): Completable {
        return firebaseFirestore
            .collection("users")
            .document(firebaseAuth.currentUser!!.uid)
            .collection("tasks")
            .document(task.id!!)
            .delete()
            .toCompletable()
    }

    override fun updateTask(task: Task): Completable {

        return firebaseFirestore
            .collection("users")
            .document(firebaseAuth.currentUser!!.uid)
            .collection("tasks")
            .document(task.id!!)
            .update(
                mapOf(
                    "taskTitle" to task.taskTitle,
                    "taskDescription" to task.taskDescription,
                    "taskStatus" to task.taskStatus
                )
            )
            .toCompletable()
    }
}