package com.example.todofirebase.data

import com.example.todofirebase.BuildConfig
import com.example.todofirebase.util.extensions.toSingle
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import io.reactivex.Single
import javax.inject.Inject

class AppRepository @Inject constructor(
    private val firebaseRemoteConfig: FirebaseRemoteConfig
) {

    fun checkFirebaseRemoteConfig(): Single<Boolean> {

        val configSettings = FirebaseRemoteConfigSettings.Builder()
            .setDeveloperModeEnabled(BuildConfig.DEBUG)
            .setMinimumFetchIntervalInSeconds(3600)
            .build()

        firebaseRemoteConfig.setConfigSettings(configSettings)

        return firebaseRemoteConfig.fetchAndActivate()
            .toSingle()
    }

    fun showNotification(){

    }
}