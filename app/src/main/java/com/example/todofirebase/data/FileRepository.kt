package com.example.todofirebase.data

import android.content.Context
import android.net.Uri
import android.util.Log
import com.example.todofirebase.util.extensions.toFlowable
import com.example.todofirebase.util.extensions.toSingle
import com.google.firebase.storage.FirebaseStorage
import io.reactivex.Single
import java.io.File
import java.io.FileInputStream
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

private val TAG = FileRepository::class.java.simpleName

class FileRepository @Inject constructor(
    private val context: Context,
    private val firebaseStorage: FirebaseStorage
) {

    fun createImageFile(extension: String = "jpg"): File? {
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH)
        val storageDir = context.filesDir
        return File.createTempFile(
            "JPEG_${timeStamp}_",
            ".$extension",
            storageDir
        )
    }

    fun saveFile(fileInputStream: FileInputStream): File {
        val attachmentFile = createImageFile()
        val outPutStream = attachmentFile!!.outputStream()
        val buffer = ByteArray(1024)

        var length = fileInputStream.read(buffer)

        while (length > 0) {
            outPutStream.write(buffer)
            length = fileInputStream.read(buffer)
        }

        outPutStream.close()
        fileInputStream.close()

        return attachmentFile
    }

    fun uploadImage(file: File): Single<Uri> {
        val localFile = Uri.fromFile(file)
        Log.d(TAG, "Local File: $localFile")

        val storageRef = firebaseStorage.reference
        val riversRef = storageRef.child("images/${file.name}")
        val uploadTask = riversRef.putFile(localFile)

        return uploadTask
            .toSingle()
            .flatMap {
                Single.defer {
                    Log.d(TAG,"Uri : ${it.storage.downloadUrl}")
                    it.storage.downloadUrl.toSingle()
                }
            }
    }
}