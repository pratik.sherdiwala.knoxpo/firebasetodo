package com.example.todofirebase.data

import com.example.todofirebase.model.Task
import io.reactivex.Completable
import io.reactivex.Single

class FakeTestRepository : ITaskRepository {
    override fun addTask(task: Task): Completable {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getAllTask(): Single<List<Task>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun deleteTask(task: Task): Completable {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun updateTask(task: Task): Completable {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}