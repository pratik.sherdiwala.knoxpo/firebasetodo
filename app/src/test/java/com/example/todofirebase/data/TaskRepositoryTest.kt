package com.example.todofirebase.data

import com.example.todofirebase.model.Task
import com.google.firebase.Timestamp
import org.junit.Before
import org.junit.Test

class TaskRepositoryTest {

    private lateinit var taskList: List<Task>
    private lateinit var taskRepository: FakeDataSource

    @Before
    fun createTask() {
        val task1 = Task().apply {
            taskTitle = "Task1"
            taskDescription = "Hello"
            taskStatus = true
            date = Timestamp.now()
            id = "123"
            photoUrl = "sjvcdxs cbvnsc"
        }

        val task2 = Task().apply {
            taskTitle = "Task1"
            taskDescription = "Hello"
            taskStatus = true
            date = Timestamp.now()
            id = "123"
            photoUrl = "sjvcdxs cbvnsc"
        }

        val task3 = Task().apply {
            taskTitle = "Task1"
            taskDescription = "Hello"
            taskStatus = true
            date = Timestamp.now()
            id = "123"
            photoUrl = "sjvcdxs cbvnsc"
        }

        taskList = listOf(task1, task2, task3).sortedBy { it.id }
        taskRepository = FakeDataSource(taskList.toMutableList())
    }

    @Test
    fun addTask() {
        taskRepository.addTask(taskList[0])
            .subscribe(
                {
                    print("Task added")
                },
                {
                    print(it.printStackTrace())
                }
            )
    }
}