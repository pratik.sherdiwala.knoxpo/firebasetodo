package com.example.todofirebase.data

import com.example.todofirebase.model.Task
import io.reactivex.Completable
import io.reactivex.Single

class FakeDataSource(
    var tasks: MutableList<Task>? = mutableListOf()
) : DataSource {

    override fun addTask(task: Task): Completable {
        val result = tasks?.add(task)

        return if (result == true) {
            Completable.complete()
        } else {
            Completable.error(Exception("Task Not added"))
        }
    }

    override fun getAllTask(): Single<List<Task>> {
        tasks?.let {
            return Single.just(it)
        }
        return Single.error(Exception("List is empty"))
    }

    override fun deleteTask(task: Task): Completable {
        val result = tasks?.contains(task)
        return if (result == true) {
            Completable.complete()
        } else {
            Completable.error(Exception("Task does not delete"))
        }
    }

    override fun updateTask(task: Task): Completable {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}